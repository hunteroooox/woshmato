package datalayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Taak;

/**<p>This class is used to send a packetData to the database.</p>
 * 
 * @author Jos Rijborz
 */
public class DAOTaak extends DAOConnection{

	/**This class (singleton pattern).*/
	private static DAOTaak instance;
	
	/**Arraylist met alle taken uit de database*/
	private ArrayList<Taak> taken = new ArrayList<Taak>();

	public DAOTaak() {

	}
	
	/**Deze methode gooit alle taken uit de database in een ArrayList.
	 *
	 * @author Jos Rijborz
	 */
	public void laadAlleTaken()
	{
		Statement st;
		Taak t;
		
		try {
			openConn();
			st = conn.createStatement();
			String sql = "SELECT * FROM Taak";
			
			ResultSet rs = st.executeQuery(sql);
			
			while (rs.next()) {
				t = new Taak();
				t.setId(rs.getInt("id"));
				t.setNaam(rs.getString("naam"));
				t.setBeschrijving(rs.getString("beschrijving"));
				t.setLesuur(rs.getInt("lesuur"));
				t.setKlokuur(rs.getInt("klokuur"));
				t.setArchived(rs.getInt("archived"));
				
				taken.add(t);
			}
			st.close();
			conn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**Deze methode maakt een nieuwe taak aan in de database.
	 *
	 * @author Jos Rijborz
	 */
	public void createTaak(Taak t) {

		/**Initial query start */
		String query = "INSERT INTO Taak (naam,beschrijving,lesuur,klokuur) VALUES ('"
				+ t.getNaam() + "','"
						+ t.getBeschrijving() + "','"
								+ t.getLesuur() + "','"
										+ t.getKlokuur() + "');";
		
		System.err.println("Hey " + query);
		try {
			openConn();

			Statement statement = conn.createStatement();
			statement.executeUpdate(query);

			statement.close();
			conn.close();

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		System.out.println("Nieuwe taak toegevoegd aan de database");
	}
	
	/**Deze methode wijzigt een bestaande taak.
	 *
	 * @author Jos Rijborz
	 */
	public void editTaak(Taak t) {

		/**Initial query start */
		String query = "UPDATE Taak SET naam='" + t.getNaam() + "', beschrijving='" + t.getBeschrijving() + "'"
				+ ", lesuur='" + t.getLesuur() + "', klokuur='" + t.getKlokuur() + "'"
						+ ", archived='" + t.getArchived() + "' WHERE id='" + t.getId() + "';";
		
		try {
			openConn();

			Statement statement = conn.createStatement();
			statement.executeUpdate(query);

			statement.close();
			conn.close();

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		System.out.println("Taak is gewijzigd");
	}
	
	/**Deze methode archiveert een taak.
	 *
	 * @author Jos Rijborz
	 */
	public void archiveTaak(Taak t) {

		/**Initial query start */
		String query = "UPDATE Taak SET archived='1' WHERE id='" + t.getId() + "';";
		
		try {
			openConn();

			Statement statement = conn.createStatement();
			statement.executeUpdate(query);

			statement.close();
			conn.close();

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		System.out.println("Taak gearchiveerd");
	}

	public static DAOTaak getInstance() {
		if(instance == null) {
			instance = new DAOTaak();
		}
		return instance;
	}

	public ArrayList<Taak> getTaken() {
		return taken;
	}

	public void setTaken(ArrayList<Taak> taken) {
		this.taken = taken;
	}
	
}
