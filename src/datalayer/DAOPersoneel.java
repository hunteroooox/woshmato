package datalayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Personeel;


/**<p>This class is used to send a packetData to the database.</p>
 * 
 * @author Jari Scholten
 */
public class DAOPersoneel extends DAOConnection{

	/**This class (singleton pattern).*/
	private static DAOPersoneel instance;
	
	/**Arraylist met alle personeel uit de database*/
	private ArrayList<Personeel> persons = new ArrayList<Personeel>();

	public DAOPersoneel() {

	}
	
	/**Deze methode gooit alle personeel uit de database in een ArrayList.
	 *
	 * @author Jari Scholten
	 */
	public void loadAllPersons()
	{
		Statement st;
		Personeel p;
		
		try {
			openConn();
			st = conn.createStatement();
			String sql = "SELECT * FROM Personeel";
			
			ResultSet rs = st.executeQuery(sql);
			
			while (rs.next()) {
				p = new Personeel();
				p.setId(rs.getInt("id"));
				p.setNaam(rs.getString("naam"));
				p.setAchternaam(rs.getString("achternaam"));
				p.setAfkorting(rs.getString("afkorting"));
				p.setFTE(rs.getInt("fte"));
				
				persons.add(p);
			}
			st.close();
			conn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**Deze methode maakt een nieuwe person aan in de database.
	 *
	 * @author Jari Scholten
	 */
	public void createPerson(Personeel p) {

		/**Initial query start */
		String query = "INSERT INTO Personeel (naam,achternaam,afkorting,fte) VALUES ('"
					+ p.getNaam() + "','"
						+ p.getAchternaam() + "','"
								+ p.getAfkorting() + "','"
										+ p.getFTE() + "');";
		
		try {
			openConn();

			Statement statement = conn.createStatement();
			statement.executeUpdate(query);

			statement.close();
			conn.close();

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		System.out.println("Nieuw Persoon toegevoegd aan de database");
	}
	
	/**Deze methode wijzigt een bestaande person.
	 *
	 * @author Jari Scholten
	 */
	public void editPerson(Personeel p) {

		/**Initial query start */
		String query = "UPDATE Taak SET naam='" + p.getNaam() + "', achternaam='" + p.getAchternaam() + "'"
				+ ", afkorting='" + p.getAfkorting() + "', fte='" + p.getFTE() + "'"
						 + "' WHERE id='" + p.getId() + "';";
		
		try {
			openConn();

			Statement statement = conn.createStatement();
			statement.executeUpdate(query);

			statement.close();
			conn.close();

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		System.out.println("Person is gewijzigd");
	}
	
	/**Deze methode archiveert een person.
	 *
	 * @author Jari Scholten
	 */
	public void archivePerson(Personeel p) {

		/**Initial query start */
		String query = "UPDATE Personeel SET archived='1' WHERE id='" + p.getId() + "';";
		
		try {
			openConn();

			Statement statement = conn.createStatement();
			statement.executeUpdate(query);

			statement.close();
			conn.close();

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		System.out.println("Person gearchiveerd");
	}

	public static DAOPersoneel getInstance() {
		if(instance == null) {
			instance = new DAOPersoneel();
		}
		return instance;
	}

	public ArrayList<Personeel> getPersoneel() {
		return persons;
	}

	public void setPersoneel(ArrayList<Personeel> persons) {
		this.persons = persons;
	}
	
}