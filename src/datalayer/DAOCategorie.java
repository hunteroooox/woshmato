package datalayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Categorie;

/**<p>This class is used to send a packetData to the database.</p>
 * 
 * @author Jos Rijborz
 */
public class DAOCategorie extends DAOConnection{

	/**This class (singleton pattern).*/
	private static DAOCategorie instance;
	
	/**Arraylist met alle categorieŽn uit de database*/
	private ArrayList<Categorie> categorieŽn = new ArrayList<Categorie>();

	public DAOCategorie() {
	}
	
	/**Deze methode gooit alle categorieŽn uit de database in een ArrayList.
	 *
	 * @author Jos Rijborz
	 */
	public void laadAlleCategorieŽn()
	{
		Statement st;
		Categorie c;
		
		try {
			openConn();
			st = conn.createStatement();
			String sql = "SELECT * FROM Categorie";
			
			ResultSet rs = st.executeQuery(sql);
			
			while (rs.next()) {
				c = new Categorie();
				c.setId(rs.getInt("id"));
				c.setNaam(rs.getString("naam"));
				
				categorieŽn.add(c);
			}
			st.close();
			conn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**Deze methode maakt een nieuwe categorie aan in de database.
	 *
	 * @author Jos Rijborz
	 */
	public void createCategorie(Categorie c) {

		/**Initial query start */
		String query = "INSERT INTO Categorie (naam) VALUES ('" + c.getNaam() + "');";
		
		try {
			openConn();

			Statement statement = conn.createStatement();
			statement.executeUpdate(query);

			statement.close();
			conn.close();

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		System.out.println("Nieuwe categorie toegevoegd aan de database");
	}
	
	/**Deze methode wijzigt een bestaande categorie.
	 *
	 * @author Jos Rijborz
	 */
	public void editCategorie(Categorie c) {

		/**Initial query start */
		String query = "UPDATE Categorie SET naam='" + c.getNaam() + "' WHERE id='" + c.getId() + "';";
		
		try {
			openConn();

			Statement statement = conn.createStatement();
			statement.executeUpdate(query);

			statement.close();
			conn.close();

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		System.out.println("Categorie is gewijzigd");
	}
	
	/**Deze methode verwijderd een taak.
	 *
	 * @author Jos Rijborz
	 */
	public void removeCategorie(Categorie c) {

		/**Initial query start */
		String query = "DELETE FROM Categorie WHERE id='" + c.getId() + "';";
		
		try {
			openConn();

			Statement statement = conn.createStatement();
			statement.executeUpdate(query);

			statement.close();
			conn.close();

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		System.out.println("Taak gearchiveerd");
	}

	public static DAOCategorie getInstance() {
		if(instance == null) {
			instance = new DAOCategorie();
		}
		return instance;
	}

	public ArrayList<Categorie> getCategorieŽn() {
		return categorieŽn;
	}

	public void setCategorie(ArrayList<Categorie> categorieŽn) {
		this.categorieŽn = categorieŽn;
	}
	
}