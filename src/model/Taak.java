package model;

import datalayer.DAOTaak;

public class Taak {

	private int id;
	private String naam;
	private String beschrijving;
	private int lesuur;
	private int klokuur;
	private int archived;
	
	public Taak(int id, String naam, String beschrijving, int lesuur, int klokuur, int archived) {
		this.id = id;
		this.naam = naam;
		this.beschrijving = beschrijving;
		this.lesuur = lesuur;
		this.klokuur = klokuur;
		this.archived = archived;
	}
	
	public Taak(String naam, String beschrijving, int lesuur, int klokuur, int archived) {
		this.naam = naam;
		this.beschrijving = beschrijving;
		this.lesuur = lesuur;
		this.klokuur = klokuur;
		this.archived = archived;
	}
	
	public Taak() {
	}
	
	public void edit()
	{
		DAOTaak.getInstance().editTaak(this);
	}
	
	public void archive()
	{
		DAOTaak.getInstance().archiveTaak(this);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getBeschrijving() {
		return beschrijving;
	}

	public void setBeschrijving(String beschrijving) {
		this.beschrijving = beschrijving;
	}

	public int getLesuur() {
		return lesuur;
	}

	public void setLesuur(int lesuur) {
		this.lesuur = lesuur;
	}

	public int getKlokuur() {
		return klokuur;
	}

	public void setKlokuur(int klokuur) {
		this.klokuur = klokuur;
	}

	public int getArchived() {
		return archived;
	}

	public void setArchived(int archived) {
		this.archived = archived;
	}
}
