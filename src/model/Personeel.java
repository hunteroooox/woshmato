package model;

public class Personeel {

	public int id;
	public String naam;
	public String achternaam;
	public String afkorting;
	public double FTE;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}

	public String getAfkorting() {
		return afkorting;
	}

	public void setAfkorting(String afkorting) {
		this.afkorting = afkorting;
	}

	public double getFTE() {
		return FTE;
	}

	public void setFTE(double fTE) {
		FTE = fTE;
	}
	
	public Personeel(int id, String naam, String achternaam, String afkorting, double FTE){
		this.id = id;
		this.naam = naam;
		this.achternaam = achternaam;
		this.afkorting = afkorting;
		this.FTE = FTE;
	}

	public Personeel() {
		
	}
	public void createPersoneel() {
		
	}
	
	public void editPersoneel() {
	
	}
	public void deletePersoneel() {
		
	}
}
