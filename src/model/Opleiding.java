package model;

public class Opleiding {

	int id;
	String klas;
	int aantal_Leerlingen;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKlas() {
		return klas;
	}

	public void setKlas(String klas) {
		this.klas = klas;
	}

	public int getAantal_Leerlingen() {
		return aantal_Leerlingen;
	}

	public void setAantal_Leerlingen(int aantal_Leerlingen) {
		this.aantal_Leerlingen = aantal_Leerlingen;
	}

	public Opleiding() {

	}
	
	public void createOpleiding(){
		
	}
	
	public void editOpleiding(){
		
	}
	
	public void deleteOpleiding(){
		
	}

}
