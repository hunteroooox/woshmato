package model;

public class Werkverdeling {
	
	public String formule;
	public int berekeningen;

	public String getFormule() {
		return formule;
	}

	public void setFormule(String formule) {
		this.formule = formule;
	}

	public int getBerekeningen() {
		return berekeningen;
	}

	public void setBerekeningen(int berekeningen) {
		this.berekeningen = berekeningen;
	}

	public Werkverdeling() {
		
	}

}
